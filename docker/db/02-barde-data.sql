SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `barde`;

TRUNCATE `artist`;
INSERT INTO `artist` (`id`, `name`, `created_at`) VALUES
('7d86badf-0e46-11ef-86da-0242ac190005',	'Richard Wagner',	'2024-05-09 20:55:38'),
('920f9186-0e46-11ef-86da-0242ac190005',	'Piotr Ilitch Tchaïkovski',	'2024-05-09 20:56:13'),
('36413328-0e47-11ef-86da-0242ac190005',	'Frédéric Chopin',	'2024-05-09 21:00:48'),
('44739d50-0e47-11ef-86da-0242ac190005',	'Wolfgang Amadeus Mozart',	'2024-05-09 21:01:12'),
('41753092-0deb-11ef-bc6a-0242ac1b0005',	'Antonio Vivaldi',	'2024-05-09 10:02:33');

TRUNCATE `media`;
INSERT INTO `media` (`id`, `type`, `label`, `file`, `content_type`, `length`, `md5sum`, `creator_id`, `enabled`, `created_at`) VALUES
('fc841894-0e47-11ef-86da-0242ac190005',	'song',	'La marche turque',	'/medias/mozart/la_marche_turque.ogg',	'audio/ogg',	8210815,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 21:06:21'),
('4b760503-0e48-11ef-86da-0242ac190005',	'song',	'Swan lake theme',	'/medias/tchaikovsky/swan_lake_theme.ogg',	'audio/ogg',	6867817,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 21:08:33'),
('8990a287-0e48-11ef-86da-0242ac190005',	'song',	'Ride of valkyries',	'/medias/wagner/ride_of_valkyries.ogg',	'audio/ogg',	10870813,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 21:10:18'),
('21b45723-0e4b-11ef-86da-0242ac190005',	'song',	'Spring',	'/medias/vivaldi/spring.ogg',	'audio/ogg',	8426578,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 21:28:52'),
('21695495-0deb-11ef-bc6a-0242ac1b0005',	'playlist',	'Demo',	'/images/playlists/amadeus.png',	'image/png',	699155,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 10:01:40'),
('733fcf31-0deb-11ef-bc6a-0242ac1b0005',	'song',	'Nocturne op. 9 no. 2',	'/medias/chopin/nocturne_op_9_no_2.ogg',	'audio/ogg',	13885097,	NULL,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 10:03:57');

TRUNCATE `playlist`;
INSERT INTO `playlist` (`id`, `name`, `image_id`, `description`, `player`, `enabled`, `created_at`) VALUES
('2169c912-0deb-11ef-bc6a-0242ac1b0005',	'Demo',	'21695495-0deb-11ef-bc6a-0242ac1b0005',	'Demo playlist',	'simple',	1,	'2024-05-09 10:01:40');


TRUNCATE `playlist_comment`;

TRUNCATE `playlist_vote`;

TRUNCATE `song`;
INSERT INTO `song` (`id`, `title`, `artist_id`, `file_id`, `url`, `duration`, `playlist_id`, `position`, `proposer_id`, `show_video`, `created_at`) VALUES
('e9ec8409-0e49-11ef-86da-0242ac190005',	'Swan lake theme',	'920f9186-0e46-11ef-86da-0242ac190005',	'4b760503-0e48-11ef-86da-0242ac190005',	'',	179,	'2169c912-0deb-11ef-bc6a-0242ac1b0005',	500,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	0,	'2024-05-09 10:02:33'),
('5d7c978b-0e4a-11ef-86da-0242ac190005',	'Nocturne op. 9 no. 2',	'36413328-0e47-11ef-86da-0242ac190005',	'733fcf31-0deb-11ef-bc6a-0242ac1b0005',	'',	269,	'2169c912-0deb-11ef-bc6a-0242ac1b0005',	100,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	0,	'2024-05-09 21:23:23'),
('aa5ad841-0e4a-11ef-86da-0242ac190005',	'Ride of valkyries',	'7d86badf-0e46-11ef-86da-0242ac190005',	'8990a287-0e48-11ef-86da-0242ac190005',	'',	300,	'2169c912-0deb-11ef-bc6a-0242ac1b0005',	300,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	0,	'2024-05-09 21:25:32'),
('5e04b22a-0e4b-11ef-86da-0242ac190005',	'Spring',	'41753092-0deb-11ef-bc6a-0242ac1b0005',	'21b45723-0e4b-11ef-86da-0242ac190005',	'',	222,	'2169c912-0deb-11ef-bc6a-0242ac1b0005',	400,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	0,	'2024-05-09 21:30:33'),
('417588fb-0deb-11ef-bc6a-0242ac1b0005',	'La marche turque',	'44739d50-0e47-11ef-86da-0242ac190005',	'fc841894-0e47-11ef-86da-0242ac190005',	'',	226,	'2169c912-0deb-11ef-bc6a-0242ac1b0005',	200,	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	0,	'2024-05-09 10:02:33');

TRUNCATE `song_vote`;
INSERT INTO `song_vote` (`song_id`, `user_id`, `vote`, `created_at`) VALUES
('e9ec8409-0e49-11ef-86da-0242ac190005',	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 21:33:52'),
('417588fb-0deb-11ef-bc6a-0242ac1b0005',	'd07e10e1-0dea-11ef-b66f-0242ac1b0005',	1,	'2024-05-09 10:07:41');

TRUNCATE `user`;
INSERT INTO `user` (`id`, `alias`, `avatar_id`, `username`, `password`, `level`, `privacy`, `skip_disliked`, `created_at`) VALUES
('d07e10e1-0dea-11ef-b66f-0242ac1b0005',	'Administrator',	NULL,	'admin',	MD5('changeme_asap'),	255,	'private',	0,	'2024-05-09 09:59:24'),
('d07e1366-0dea-11ef-b66f-0242ac1b0005',	'DJ',	NULL,	'publisher',	MD5('changeme'),	200,	'private',	0,	'2024-05-09 09:59:24'),
('d07e1429-0dea-11ef-b66f-0242ac1b0005',	'Citizen',	NULL,	'test',	MD5('changeme'),	10,	'private',	0,	'2024-05-09 09:59:24'),
('d07e1492-0dea-11ef-b66f-0242ac1b0005',	'Guest',	NULL,	'guest',	MD5('changeme'),	1,	'private',	0,	'2024-05-09 09:59:24');
