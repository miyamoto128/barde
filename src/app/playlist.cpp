#include "playlist.h"

#include <cppcms/http_file.h>
#include <cppcms/url_dispatcher.h>
#include <booster/log.h>

#include <app/util/stringMethods.h>

#include <data/mediaMapper.h>
#include <data/playlistMapper.h>
#include <data/playlistCommentMapper.h>
#include <data/songMapper.h>

#include <sstream>

using namespace cppcms::http;

namespace app {
    const std::string Playlist::TOP_LIST_ID = "top";
    const std::string Playlist::TOP_LIST_NAME = "Meta-Playlist";
    const std::string Playlist::TOP_LIST_IMAGE = "/images/playlists/top.png";

    const std::string Playlist::WORST_LIST_ID = "worst";
    const std::string Playlist::WORST_LIST_NAME = "Anti-Playlist";
    const std::string Playlist::WORST_LIST_IMAGE = "/images/playlists/worst.png";

    const std::string Playlist::RANDOM_LIST_ID = "random";
    const std::string Playlist::RANDOM_LIST_NAME = "Random songs";
    const std::string Playlist::RANDOM_LIST_IMAGE = "/images/playlists/random.png";

    const std::string Playlist::PROPOSED_LIST_NAME = "Proposed songs";

    Playlist::Playlist(cppcms::service& s) :
        app::Master(s)
    {
        dispatcher().map("", &Playlist::displayCurrent, this);
        mapper().assign("");
        
        dispatcher().assign("/all", &Playlist::displayAll, this);
        mapper().assign("");

        dispatcher().assign("/top", &Playlist::displayTop, this);
        mapper().assign("");

        dispatcher().assign("/worst", &Playlist::displayWorst, this);
        mapper().assign("");

        dispatcher().assign("/random", &Playlist::displayRandom, this);
        mapper().assign("");

        dispatcher().assign("/new", &Playlist::displayNew, this);
        mapper().assign("");

        dispatcher().assign("/edit/([a-f0-9-]+)", &Playlist::displayEdit, this, 1);
        mapper().assign("/{1}");

        dispatcher().assign("/([a-f0-9-]+)", &Playlist::display, this, 1);
        mapper().assign("/{1}");
    }

    void Playlist::displayCurrent() {
        BOOSTER_DEBUG(__func__);

        data::PlaylistMapper playlistMapper(connectionString_);
        std::string playlistId = playlistMapper.findCurrentPlaylistId();

        if (playlistId.empty()) {
            playlistId = TOP_LIST_ID;
            BOOSTER_WARNING(__func__) << "No current playlist, falling back on " << playlistId;
        }
        display(playlistId);
    }

    void Playlist::display(std::string playlistId) {
        BOOSTER_DEBUG(__func__) << "playlist ID: " << playlistId;

        data::PlaylistPage playlist(page_);

        if (!checkAuth(playlist.user)) {
            forbidAccess(playlist.user);
            return;
        }

        std::string key = getCacheKey(playlistId, playlist.user);
        if (cache().fetch_page(key)) {
            BOOSTER_DEBUG(__func__) << "Fetch cache for key=" << key;
            return;
        }

        data::PlaylistMapper playlistMapper(connectionString_);
        playlistMapper.loadPlaylist(playlist, playlistId, playlist.user);

        data::PlaylistCommentMapper playlistCommentMapper(connectionString_);
        playlistCommentMapper.loadComments(playlist, playlistId);
        playlistCommentMapper.loadUserNbComments(playlist.user);

        doDisplay(playlist);

        cache().add_trigger(playlist.user.id);
        cache().store_page(key, CACHE_TTL_MEDIUM);
        BOOSTER_DEBUG(__func__) << "Store cache for key=" << key;
    }

    void Playlist::displayAll() {
        BOOSTER_DEBUG(__func__);

        data::AllPlaylistsPage allPlaylists(page_);

        if (! checkAuth(allPlaylists.user)) {
            forbidAccess(allPlaylists.user);
            return;
        }

        std::string key = getCacheKey("ALL", allPlaylists.user);
        if (cache().fetch_page(key)) {
            BOOSTER_DEBUG(__func__) << "Fetch cache for key=" << key;
            return;
        }

        data::PlaylistMapper playlistMapper(connectionString_);
        playlistMapper.loadAllPlaylists(allPlaylists);

        data::SongMapper songMapper(connectionString_);
        songMapper.loadUserProposedSongs(allPlaylists.user);

        data::PlaylistCommentMapper playlistCommentMapper(connectionString_);
        playlistCommentMapper.loadUserNbComments(allPlaylists.user);

        render("allPlaylists", allPlaylists);

        cache().add_trigger(allPlaylists.user.id);
        cache().store_page(key, CACHE_TTL_LONG);
        BOOSTER_DEBUG(__func__) << "Store cache for key=" << key;
    }

    void Playlist::displayTop() {
        BOOSTER_DEBUG(__func__);

        data::PlaylistPage playlistPage(page_);

        if (! checkAuth(playlistPage.user)) {
            forbidAccess(playlistPage.user);
            return;
        }

        std::string key = getCacheKey(TOP_LIST_ID, playlistPage.user);
        if (cache().fetch_page(key)) {
            BOOSTER_DEBUG(__func__) << "Fetch cache for key=" << key;
            return;
        }

        playlistPage.playlist.id = TOP_LIST_ID;
        playlistPage.playlist.name = TOP_LIST_NAME;
        playlistPage.playlist.image.file = TOP_LIST_IMAGE;
        playlistPage.autoGenerated = true;

        data::PlaylistMapper playlistMapper(connectionString_);
        playlistMapper.loadTopPlaylist(playlistPage, playlistPage.user, TOP_LIST_NB_SONGS,
                data::PlaylistMapper::OrderBy::DESC);

        data::PlaylistCommentMapper playlistCommentMapper(connectionString_);
        playlistCommentMapper.loadUserNbComments(playlistPage.user);

        doDisplay(playlistPage);

        cache().add_trigger(playlistPage.user.id);
        cache().store_page(key, CACHE_TTL_SHORT);
        BOOSTER_DEBUG(__func__) << "Store cache for key=" << key;
    }

    void Playlist::displayWorst() {
        BOOSTER_DEBUG(__func__);

        data::PlaylistPage playlistPage(page_);

        if (! checkAuth(playlistPage.user)) {
            forbidAccess(playlistPage.user);
            return;
        }

        std::string key = getCacheKey(WORST_LIST_ID, playlistPage.user);
        if (cache().fetch_page(key)) {
            BOOSTER_DEBUG(__func__) << "Fetch cache for key=" << key;
            return;
        }

        playlistPage.playlist.id = WORST_LIST_ID;
        playlistPage.playlist.name = WORST_LIST_NAME;
        playlistPage.playlist.image.file = WORST_LIST_IMAGE;
        playlistPage.autoGenerated = true;

        data::PlaylistMapper playlistMapper(connectionString_);
        playlistMapper.loadTopPlaylist(playlistPage, playlistPage.user, TOP_LIST_NB_SONGS,
                data::PlaylistMapper::OrderBy::ASC);

        data::PlaylistCommentMapper playlistCommentMapper(connectionString_);
        playlistCommentMapper.loadUserNbComments(playlistPage.user);

        doDisplay(playlistPage);

        cache().add_trigger(playlistPage.user.id);
        cache().store_page(key, CACHE_TTL_SHORT);
        BOOSTER_DEBUG(__func__) << "Store cache for key=" << key;
    }

    void Playlist::displayRandom() {
        BOOSTER_DEBUG(__func__);

        data::PlaylistPage playlistPage(page_);

        if (! checkAuth(playlistPage.user)) {
            forbidAccess(playlistPage.user);
            return;
        }

        std::string key = getCacheKey(RANDOM_LIST_ID, playlistPage.user);
        if (cache().fetch_page(key)) {
            BOOSTER_DEBUG(__func__) << "Fetch cache for key=" << key;
            return;
        }

        playlistPage.playlist.id = RANDOM_LIST_ID;
        playlistPage.playlist.name = RANDOM_LIST_NAME;
        playlistPage.playlist.image.file = RANDOM_LIST_IMAGE;
        playlistPage.autoGenerated = true;

        data::PlaylistMapper playlistMapper(connectionString_);
        playlistMapper.loadUserTopPlaylist(playlistPage, playlistPage.user, TOP_LIST_NB_SONGS,
                data::PlaylistMapper::OrderBy::RAND);

        data::PlaylistCommentMapper playlistCommentMapper(connectionString_);
        playlistCommentMapper.loadUserNbComments(playlistPage.user);

        doDisplay(playlistPage);

        cache().add_trigger(playlistPage.user.id);
        cache().store_page(key, CACHE_TTL_MEDIUM);
        BOOSTER_DEBUG(__func__) << "Store cache for key=" << key;
    }

    void Playlist::displayNew() {
        BOOSTER_DEBUG(__func__);

        data::NewPlaylistPage newPlaylist(page_);

        if (! checkAuth(newPlaylist.user, data::User::ADMINISTRATOR)) {
            forbidAccess(newPlaylist.user);
            return;
        }

        data::PlaylistMapper playlistMapper(connectionString_);

        if (request().request_method() == "POST") {
            newPlaylist.input.load(context());
            if(! newPlaylist.input.validate()) {
                newPlaylist.alerts.errors.push_back("Invalid or missing fields!");
            } else {
                data::PlaylistItem playlist;

                playlist.name = newPlaylist.input.name.value();
                if (!newPlaylist.input.imageId.value().empty()) {
                    playlist.image.id = newPlaylist.input.imageId.value();
                }

                file* imageFile = newPlaylist.input.newImage.value().get();
                std::string imagePath = toUploadRelativePath(imageFile);

                playlist.description = newPlaylist.input.description.value();

                std::ostringstream msg;
                try {
                    // Upload and insert image
                    std::string imageFullPath = toFullPath(imagePath);
                    imageFile->save_to(imageFullPath);
                    BOOSTER_INFO(__func__) << "Uploaded file " << imageFullPath;

                    data::MediaMapper mediaMapper(connectionString_);
                    data::PlaylistMapper playlistMapper(connectionString_);

                    data::Media media(data::Media::TYPE_PLAYLIST, playlist.name, imagePath,
                            imageFile);

                    std::string lastInsertId = mediaMapper.insert(newPlaylist.user.id, media);
                    if (lastInsertId.empty()) {
                        msg << "Could not create media " << imagePath
                            << " for playlist " << playlist.name;
                        newPlaylist.alerts.warnings.push_back(msg.str());
                        BOOSTER_WARNING(__func__) << msg.str();
                    } else {
                        if (playlist.hasImage()) {
                            mediaMapper.disable(playlist.image.id);
                        }
                        playlist.image.id = lastInsertId;
                    }

                    // Insert playlist
                    if (!playlistMapper.insert(playlist)) {
                        msg << "Could not create playlist " << playlist.name;
                        newPlaylist.alerts.errors.push_back(msg.str());
                        BOOSTER_ERROR(__func__) << msg.str();
                    } else {
                        msg << "Successfully created playlist " << playlist.name;
                        newPlaylist.alerts.success.push_back(msg.str());
                        BOOSTER_INFO(__func__) << msg.str();

                        newPlaylist.input.clear();
                    }
                } catch(const cppcms::cppcms_error& e) {
                    msg << "Could not upload file " << imagePath;
                    newPlaylist.alerts.errors.push_back(msg.str());
                    BOOSTER_ERROR(__func__) << msg.str() << " - - " << e.trace();
                }
            }
        }

        newPlaylist.pageTitle = translate("New playlist");

        render("newPlaylist", newPlaylist);
    }

    void Playlist::displayEdit(std::string playlistId) {
        BOOSTER_DEBUG(__func__);

        data::EditPlaylistPage editPlaylist(page_);

        if (! checkAuth(editPlaylist.user, data::User::ADMINISTRATOR)) {
            forbidAccess(editPlaylist.user);
            return;
        }

        data::PlaylistMapper playlistMapper(connectionString_);

        if (request().request_method() == "POST") {
            editPlaylist.input.load(context());
            if(! editPlaylist.input.validate()) {
                editPlaylist.alerts.errors.push_back("Invalid or missing fields!");
            } else {
                data::PlaylistItem playlist;
                playlist.id = playlistId;

                playlist.name = editPlaylist.input.name.value();
                if (!editPlaylist.input.imageId.value().empty()) {
                    playlist.image.id = editPlaylist.input.imageId.value();
                }

                file* imageFile = 0;
                std::string imagePath;
                if (editPlaylist.input.newImage.set()) {
                    imageFile = editPlaylist.input.newImage.value().get();
                    imagePath = toUploadRelativePath(imageFile);
                } else {
                    imagePath = editPlaylist.input.image.value();
                }

                playlist.description = editPlaylist.input.description.value();

                std::ostringstream msg;
                try {
                    // Upload and insert image
                    if (imageFile != 0) {
                        std::string imageFullPath = toFullPath(imagePath);
                        imageFile->save_to(imageFullPath);
                        BOOSTER_INFO(__func__) << "Uploaded file " << imageFullPath;

                        data::MediaMapper mediaMapper(connectionString_);
                        data::PlaylistMapper playlistMapper(connectionString_);

                        data::Media media(data::Media::TYPE_PLAYLIST, playlist.name, imagePath,
                                imageFile);


                        std::string lastInsertId = mediaMapper.insert(editPlaylist.user.id, media);
                        if (lastInsertId.empty()) {
                            msg << "Could not create media " << imagePath
                                << " for playlist " << playlist.name;
                            editPlaylist.alerts.warnings.push_back(msg.str());
                            BOOSTER_WARNING(__func__) << msg.str();
                        } else {
                            if (playlist.hasImage()) {
                                mediaMapper.disable(playlist.image.id);
                            }
                            playlist.image.id = lastInsertId;
                        }
                    }

                    // Insert or update playlist
                    if (!playlistMapper.update(playlist)) {
                        msg << "Could not modify playlist " << playlist.name;
                        editPlaylist.alerts.errors.push_back(msg.str());
                        BOOSTER_ERROR(__func__) << msg.str();
                    } else {
                        msg << "Successfully modified playlist " << playlist.name;
                        editPlaylist.alerts.success.push_back(msg.str());
                        BOOSTER_INFO(__func__) << msg.str();
                    }
                } catch(const cppcms::cppcms_error& e) {
                    msg << "Could not upload file " << imagePath;
                    editPlaylist.alerts.errors.push_back(msg.str());
                    BOOSTER_ERROR(__func__) << msg.str() << " - - " << e.trace();
                }
            }
        } else if (!playlistId.empty() && playlistMapper.loadPlaylist(editPlaylist.playlist, playlistId)) {
            BOOSTER_DEBUG(__func__) << "Loading fields for playlist " << playlistId;
            editPlaylist.input.name.value(editPlaylist.playlist.name);
            editPlaylist.input.imageId.value(editPlaylist.playlist.image.id);
            editPlaylist.input.image.value(editPlaylist.playlist.image.file);
            editPlaylist.input.description.value(editPlaylist.playlist.description);
        }

        editPlaylist.pageTitle = translate("Modify playlist");

        render("editPlaylist", editPlaylist);
    }

    std::string Playlist::getCacheKey(const std::string& playlistId, const data::User& user) {
        return Master::getCacheKey(CACHE_PREFIX, playlistId, user);
    }

    void Playlist::doDisplay(data::PlaylistPage& playlist) {
        playlist.pageTitle = playlist.playlist.name;

        data::SongMapper songMapper(connectionString_);
        songMapper.loadUserProposedSongs(playlist.user);

        render("playlist", playlist);
    }

}   // namespace app

