#include "playlistVote.h"

#include <cppcms/http_response.h>
#include <cppcms/json.h>
#include <cppcms/url_dispatcher.h>
#include <booster/log.h>

#include <app/playlist.h>


using namespace cppcms::http;

namespace app {
    PlaylistVote::PlaylistVote(cppcms::service& s) :
        app::Master(s)
    {
        dispatcher().map("/ajax-vote/([a-f0-9-]+)/(\\w+)", &PlaylistVote::ajaxVote, this, 1, 2);
    }

    void PlaylistVote::ajaxVote(const std::string& playlistId, const std::string& vote) {
        data::User user;

        if (! checkAuth(user, data::User::CITIZEN)) {
            response().make_error_response(response::forbidden);
            BOOSTER_WARNING(__func__) << "Forbid user "
                << user.alias << " to vote for playlist " << playlistId;
            return;
        }

        data::PlaylistVoteMapper playlistVoteMapper(connectionString_);

        bool result = playlistVoteMapper.insert(
            user.id,
            playlistId,
            data::PlaylistVote::stringToVote(vote)
        );

        cache().rise(user.id);
        BOOSTER_DEBUG(__func__) << "Clean caches for user ID " << user.id;

        cppcms::json::value jsonOutput;
        jsonOutput["success"] = result;
        response().out() << jsonOutput;
    }

}   // namespace app
