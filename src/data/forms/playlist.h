#ifndef DATA_FORM_PLAYLIST_H
#define DATA_FORM_PLAYLIST_H

#include <cppcms/form.h>


namespace data {

	struct PlaylistForm: public cppcms::form {
		cppcms::widgets::text name;
		cppcms::widgets::file newImage;
		cppcms::widgets::hidden imageId;
		cppcms::widgets::hidden image;
		cppcms::widgets::textarea description;
		cppcms::widgets::submit submit;
		cppcms::form inputs;
		cppcms::form buttons;

		PlaylistForm() {
            using booster::locale::translate;

            name.id("name");
			name.message(translate("Name:"));
			name.limits(1, 50);

			newImage.id("image");
			newImage.message(translate("Image:"));
			newImage.limits(1000, 500000);
            newImage.mime(booster::regex("image/.+"));

			imageId.id("image_id");

			image.id("image");

            description.id("description");
			description.message(translate("Description:"));
            description.cols(40);
            description.rows(4);
			description.limits(0, 500);

			submit.value(translate("Submit"));

			inputs.add(name);
			inputs.add(newImage);
			inputs.add(imageId);
			inputs.add(image);
			inputs.add(description);
			buttons.add(submit);
			add(inputs);
			add(buttons);
	}};

	struct NewPlaylistForm: public PlaylistForm {

        NewPlaylistForm() {
            newImage.non_empty();
        }
    };

	struct EditPlaylistForm: public PlaylistForm {
    };

}   // namespace data
#endif  // DATA_FORM_PLAYLIST_H
