#include "playlistMapper.h"

#include <data/songMapper.h>

#include <booster/log.h>

using namespace std::chrono;

namespace data {

    std::vector<PlaylistItem> PlaylistMapper::cachedPlaylistItems_ = {};
    time_point<system_clock> PlaylistMapper::cachedPlaylistItemsLastUpdated_ = system_clock::now();

    PlaylistMapper::PlaylistMapper(const std::string& connectionString)
        : DbMapper(connectionString)
    {}

    std::string PlaylistMapper::findCurrentPlaylistId() {
        std::string playlistId = "";

        std::string query = "SELECT id FROM playlist "
            "WHERE enabled = 1 "
            "ORDER BY created_at DESC LIMIT 1 ";

        BOOSTER_DEBUG(__func__) << query;

        cppdb::result result = connection() << query;

        if (result.next()) {
            result >> playlistId;
        }
        return playlistId;
    }

    bool PlaylistMapper::loadPlaylist(PlaylistPage& dest, const std::string& playlistId, const User& user) {
        bool success = false;

        dest.songs.clear();

        std::string query = "SELECT p.id, p.name, mp.file AS image, "
            "p.description, UNIX_TIMESTAMP(p.created_at) AS created_at, "
            "vp.vote AS vote, "
            "s.id AS song_id, s.title AS song_title, a.name AS artist_name, "
            "s.duration, s.show_video, s.url, s.file_id, ms.file, "
            "u.alias AS proposer, vs.vote AS song_vote "
            "FROM playlist p "
            "LEFT JOIN media mp ON mp.id = p.image_id "
            "LEFT JOIN song s ON s.playlist_id = p.id "
            "LEFT JOIN media ms ON ms.id = s.file_id "
            "INNER JOIN artist a ON a.id = s.artist_id "
            "LEFT JOIN user u ON u.id = s.proposer_id AND u.privacy = 'public' "
            "LEFT JOIN playlist_vote vp ON vp.playlist_id = p.id AND vp.user_id = ? "
            "LEFT JOIN song_vote vs ON vs.song_id = s.id AND vs.user_id = ? "
            "WHERE p.id = ? ";

        if (user.level < data::User::ADMINISTRATOR) {
            query += "AND p.enabled = 1 ";
        }

        if (user.skipDislikedSongs) {
            query += "AND IFNULL(vs.vote, 0) >= 0 ";
        }

        query += "ORDER BY s.position ";

        BOOSTER_DEBUG(__func__) << query << ", " << user.id <<
            ", " << user.id << ", " << playlistId;

        cppdb::result result = connection() << query << user.id << user.id << playlistId;

        while (result.next()) {
            dest.playlist.id = result.get<std::string>("id");
            dest.playlist.name = result.get<std::string>("name");
            dest.playlist.image.file = result.get<std::string>("image", "");
            dest.playlist.description = result.get<std::string>("description", "");
            dest.playlist.createdAt = result.get<time_t>("created_at");
            dest.vote.value = result.get<short>("vote", 0);

            std::string songId = result.get<std::string>("song_id", "");
            if (!songId.empty()) {
                data::Song song;

                song.id = songId;
                song.title = result.get<std::string>("song_title", "");
                song.artist.name = result.get<std::string>("artist_name", "");
                song.media.id = result.get<std::string>("file_id", "");
                song.media.file = result.get<std::string>("file", "");
                song.url = result.get<std::string>("url", "");
                song.duration = result.get<unsigned int>("duration", 0);
                song.showVideo = result.get<unsigned short>("show_video", 0);
                song.proposer = result.get<std::string>("proposer", User::ANONYMOUS_ALIAS);
                song.vote.value = result.get<short>("song_vote", 0);

                dest.songs.push_back(song);
            }
            success = true;
        }
        return success;
    }

    bool PlaylistMapper::loadPlaylist(PlaylistItem& dest, const std::string& playlistId) {
        bool success = false;

        std::string query = "SELECT p.id, p.name, p.image_id, mp.file AS image, "
            "p.description, UNIX_TIMESTAMP(p.created_at) AS created_at "
            "FROM playlist p "
            "LEFT JOIN media mp ON mp.id = p.image_id "
            "WHERE p.id = ? ";

        BOOSTER_DEBUG(__func__) << query << ", " << playlistId;

        cppdb::result result = connection() << query << playlistId;

        if (result.next()) {
            dest.id = result.get<std::string>("id");
            dest.name = result.get<std::string>("name");
            dest.image.id = result.get<std::string>("image_id", 0);
            dest.image.file = result.get<std::string>("image", "");
            dest.description = result.get<std::string>("description", "");
            dest.createdAt = result.get<time_t>("created_at");
            success = true;
        }
        return success;
    }

    bool PlaylistMapper::loadTopPlaylist(PlaylistPage& dest, const User& user, unsigned short nbSongs, OrderBy orderBy) {
        bool success = false;

        dest.songs.clear();

        std::string query="SELECT votes.song_id, SUM(votes.vote) AS total_votes, "
            "s.title AS song_title, a.name AS artist_name, s.file_id, ms.file, s.url, "
            "s.duration, s.show_video, u.alias AS proposer, sv.vote AS song_vote "
            "FROM " + SongMapper::SQL_GLOBAL_VOTES +
            "INNER JOIN song s ON s.id = votes.song_id "
            "INNER JOIN media ms ON ms.id = s.file_id "
            "INNER JOIN artist a ON a.id = s.artist_id "
            "LEFT JOIN user u ON u.id = s.proposer_id AND u.privacy = 'public' "
            "LEFT JOIN song_vote sv ON sv.song_id = s.id AND sv.user_id = ? "
            "GROUP BY song_id ";

        switch(orderBy) {
            case OrderBy::DESC:
                query += "ORDER BY total_votes DESC, song_id DESC ";
                break;
            case OrderBy::ASC:
                query += "ORDER BY total_votes ASC, song_id ASC ";
                break;
            default:
                query += "HAVING total_votes > 0 "; // Increase random playlist quality
                query += "ORDER BY RAND() ";
                break;
        }

        query += "LIMIT ? ";

        BOOSTER_DEBUG(__func__) << query << ", " << user.id << ", " << nbSongs;

        cppdb::result result = connection() << query << user.id << nbSongs;

        while (result.next()) {
            data::Song song;
            song.id = result.get<std::string>("song_id");
            song.vote.totalValues = result.get<float>("total_votes");
            song.title = result.get<std::string>("song_title", "");
            song.artist.name = result.get<std::string>("artist_name", "");
            song.media.id = result.get<std::string>("file_id", "");
            song.media.file = result.get<std::string>("file", "");
            song.url = result.get<std::string>("url", "");
            song.duration = result.get<unsigned int>("duration", 0);
            song.showVideo = result.get<unsigned short>("show_video", 0);
            song.proposer = result.get<std::string>("proposer", User::ANONYMOUS_ALIAS);
            song.vote.value = result.get<short>("song_vote", 0);
            dest.songs.push_back(song);

            success = true;
        }
        return success;
    }

    bool PlaylistMapper::loadUserTopPlaylist(PlaylistPage& dest, const User& user,  unsigned short nbSongs, OrderBy orderBy) {
        bool success = false;

        dest.songs.clear();

        std::string query="SELECT votes.song_id, SUM(votes.vote) AS total_votes, "
            "s.title AS song_title, a.name AS artist_name, s.file_id, ms.file, s.url, "
            "s.duration, s.show_video, u.alias AS proposer, "
            "sv.vote AS song_vote "
            "FROM " + SongMapper::SQL_GLOBAL_VOTES +
            "INNER JOIN song s ON s.id = votes.song_id "
            "INNER JOIN media ms ON ms.id = s.file_id "
            "INNER JOIN artist a ON a.id = s.artist_id "
            "LEFT JOIN user u ON u.id = s.proposer_id AND u.privacy = 'public' "
            "LEFT JOIN song_vote sv ON sv.song_id = s.id AND sv.user_id = ? "
            "WHERE sv.vote IS NULL OR sv.vote >= 0 "
            "GROUP BY song_id ";

        switch(orderBy) {
            case OrderBy::DESC:
                query += "ORDER BY song_vote DESC, total_votes DESC ";
                break;
            case OrderBy::ASC:
                query += "ORDER BY song_vote ASC, total_votes ASC ";
                break;
            default:
                query += "HAVING total_votes > 0 "; // Increase random playlist quality
                query += "ORDER BY song_vote DESC, RAND() ";
                break;
        }

        query += "LIMIT ? ";

        BOOSTER_DEBUG(__func__) << query << ", " << user.id << ", " << nbSongs;

        cppdb::result result = connection() << query << user.id << nbSongs;

        while (result.next()) {
            data::Song song;
            song.id = result.get<std::string>("song_id");
            song.vote.totalValues = result.get<float>("total_votes");
            song.title = result.get<std::string>("song_title", "");
            song.artist.name = result.get<std::string>("artist_name", "");
            song.media.id = result.get<std::string>("file_id", "");
            song.media.file = result.get<std::string>("file", "");
            song.url = result.get<std::string>("url", "");
            song.duration = result.get<unsigned int>("duration", 0);
            song.showVideo = result.get<unsigned short>("show_video", 0);
            song.proposer = result.get<std::string>("proposer", User::ANONYMOUS_ALIAS);
            song.vote.value = result.get<short>("song_vote", 0);
            dest.songs.push_back(song);

            success = true;
        }
        return success;
    }

    bool PlaylistMapper::loadProposedPlaylist(PlaylistPage& dest) {
        bool success = false;

        dest.songs.clear();

        std::string query = "SELECT s.id AS song_id, CONCAT(s.title, ' (', s.id, ')') AS song_title, "
            "a.name AS artist_name, s.file, s.url "
            "FROM song s "
            "LEFT JOIN playlist pl ON pl.id = s.playlist_id "
            "INNER JOIN artist a ON a.id = s.artist_id "
            "WHERE pl.id IS  NULL";

        BOOSTER_DEBUG(__func__) << query;

        cppdb::result result = connection() << query;

        while (result.next()) {
            data::Song song;
            result >> song.id >> song.title >> song.artist.name >> song.media.file >> song.url;
            dest.songs.push_back(song);

            success = true;
        }
        return success;
    }

    bool PlaylistMapper::loadAllPlaylists(AllPlaylistsPage& dest) {
        bool success = false;

        duration<double> elapsed_seconds = system_clock::now() - cachedPlaylistItemsLastUpdated_;
        if (cachedPlaylistItems_.empty() || elapsed_seconds.count() > CACHE_EXPIRATION) {

            cachedPlaylistItems_.clear();

            std::string query = "SELECT p.id, p.name, mp.file AS image, p.description, "
                "UNIX_TIMESTAMP(p.created_at) AS created_at, "
                "COUNT(p.id) AS nb_songs "
                "FROM playlist p "
                "LEFT JOIN media mp ON mp.id = p.image_id "
                "LEFT JOIN song s ON s.playlist_id = p.id "
                "WHERE p.enabled = 1 "
                "GROUP BY p.id "
                "ORDER BY p.created_at DESC ";

            BOOSTER_DEBUG(__func__) << query;

            cppdb::result result = connection() << query;

            while (result.next()) {
                data::PlaylistItem item;
                result >> item.id >> item.name >> item.image.file >> item.description
                    >> item.createdAt >> item.nbSongs;
                cachedPlaylistItems_.push_back(item);

                success = true;
            }
            cachedPlaylistItemsLastUpdated_ = system_clock::now();
        }
        dest.playlists = cachedPlaylistItems_;

        return success;
    }

    bool PlaylistMapper::loadComingPlaylists(HtmlPage& dest) {
        bool success = false;

        dest.comingPlaylists.clear();

        std::string query = "SELECT p.id, p.name, COUNT(p.id) AS nb_songs "
            "FROM playlist p "
            "LEFT JOIN song s ON s.playlist_id = p.id "
            "WHERE p.enabled = 0 "
            "GROUP BY p.id "
            "ORDER BY p.created_at ASC ";

        BOOSTER_DEBUG(__func__) << query;

        cppdb::result result = connection() << query;

        while (result.next()) {
            HtmlPage::ComingPlaylist playlist;
            result >> playlist.id >> playlist.name >> playlist.nbSongs;
            dest.comingPlaylists.push_back(playlist);

            success = true;
        }

        return success;
    }

    bool PlaylistMapper::insert(const PlaylistItem& playlist) {
        std::string query = "INSERT INTO playlist "
            "(name, image_id, description, enabled) "
            "VALUES (?, ?, ? , 0) ";

        BOOSTER_DEBUG(__func__) << query << ", " << playlist.name << ", " << playlist.image.id
            << ", " << playlist.description;

        cppdb::statement st = connection() << query <<  playlist.name << playlist.image.id
            << playlist.description
            << cppdb::exec;

        return st.affected() >= 1;
    }

    bool PlaylistMapper::update(const PlaylistItem& playlist) {
        std::string query = "UPDATE playlist SET "
            "name = ?, image_id = ?, description = ? "
            "WHERE id = ?";

        BOOSTER_DEBUG(__func__) << query << ", " << playlist.name << ", " << playlist.image.id
            << ", " << playlist.description << ", " << playlist.id;

        cppdb::statement st = connection() << query <<  playlist.name << playlist.image.id
            << playlist.description << playlist.id
            << cppdb::exec;

        return st.affected() >= 1;
    }

}   // namespace data
