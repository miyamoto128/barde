#include "mediaMapper.h"

#include <booster/log.h>


namespace data {

    MediaMapper::MediaMapper(const std::string& connectionString)
        : DbMapper(connectionString)
    {}

    bool MediaMapper::get(Media& dest, const std::string& id) {
        dest.clear();

        std::string query = "SELECT type, label, file FROM media "
            "WHERE id = ? ";

        BOOSTER_DEBUG(__func__) << query << ", " << id;

        cppdb::statement statement = connection() << query << id;
        cppdb::result result = statement.row();

        if(result.empty()) {
            return false;
        }

        dest.id = id;
        dest.type = result.get<std::string>("type");
        dest.label = result.get<std::string>("label");
        dest.file = result.get<std::string>("file");

        return true;
    }

    std::string MediaMapper::insert(const std::string& userId, const Media& media) {
        std::string query = "INSERT INTO media "
            "(type, label, file, content_type, length, creator_id, enabled) "
            "VALUES (?, ?, ?, ?, ?, ?, 1) RETURNING (id) ";

        BOOSTER_DEBUG(__func__) << query << ", " << media.type << ", " << media.label
            << ", " << media.file << ", " << media.contentType << ", " << media.length
            << ", " << userId;

        cppdb::statement statement = connection() << query << media.type << media.label
            << media.file << media.contentType << media.length << userId;

        cppdb::result result = statement.row();
        if(result.empty()) {
            return "";
        }

        return result.get<std::string>("id");
    }

    bool MediaMapper::disable(const std::string& id) {
        std::string query = "UPDATE media SET enabled = 0 WHERE id = ?";

        BOOSTER_DEBUG(__func__) << query << ", " << id;

        cppdb::statement statement = connection() << query << id
            << cppdb::exec;

        return statement.affected() >= 1;
    }

}   // namespace data
