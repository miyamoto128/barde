#ifndef DATA_USER_H
#define DATA_USER_H

#include <cppcms/view.h>

#include <data/media.h>
#include <data/song.h>

#include <string>
#include <sstream>
#include <vector>

namespace data {

    struct User : public cppcms::base_content {
        static constexpr const char* ANONYMOUS_ALIAS = "Anonymous";

        static constexpr const char* PRIVACY_PRIVATE = "private";   // User name does not appear on its proposed songs
        static constexpr const char* PRIVACY_PUBLIC = "public";

        enum Level {
            ANONYMOUS = 0,          // Not authentified
            GUEST = 1,              // Can listen to music
            CITIZEN = 10,           // Can vote and comment
            ADMINISTRATOR = 200,    // Can create and modify playlists
            ROOT = 255              // Has all permissions
        };

        std::string id;
        std::string alias;
        Media avatar;
        unsigned int level;
        bool isAuthenticated;
        bool isAllowed;
        std::string privacy;
        bool skipDislikedSongs;

        // Songs the user proposed
        std::vector<Song> proposedSongs;

        unsigned int nbComments;


        User() {
            clear();
        }

        bool isPublic() {
            return privacy == PRIVACY_PUBLIC;
        }

        bool hasAvatar() {
            return !avatar.id.empty();
        }

        void clear() {
            id = "";
            alias = ANONYMOUS_ALIAS;
            avatar.clear();
            level = data::User::ANONYMOUS;
            isAuthenticated = false;
            isAllowed = false;
            privacy = PRIVACY_PRIVATE;
            skipDislikedSongs = false;

            nbComments = 0;
        }

        std::string toString() const {
            std::ostringstream oss;
            oss << "{ id: " << id << ", alias: " << alias << ", avatar: " << avatar.id
                << ", level: " << level << ", privacy: " << privacy
                << ", skipDislikedSongs: " << skipDislikedSongs << " }";
            return oss.str();
        }
    };

}   // namespace data


#endif  // DATA_USER_H;
