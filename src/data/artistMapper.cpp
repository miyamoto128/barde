#include "artistMapper.h"

#include <booster/log.h>


namespace data {

    ArtistMapper::ArtistMapper(const std::string& connectionString)
        : DbMapper(connectionString)
    {}

    bool ArtistMapper::getByName(Artist& dest, const std::string& name) {
        std::string query = "SELECT id, name FROM artist WHERE LCASE(name) = LCASE(?) "
            "ORDER BY created_at DESC LIMIT 1 ";

        BOOSTER_DEBUG(__func__) << query << ", " << name;

        cppdb::statement statement = connection() << query << name;
        cppdb::result result = statement.row();

        if(result.empty()) {
            return false;
        }

        dest.id = result.get<std::string>("id");
        dest.name = result.get<std::string>("name");
        return true;
    }

    std::string ArtistMapper::insert(const Artist& artist) {
        std::string query = "INSERT INTO artist (name) VALUES (?) RETURNING id";

        BOOSTER_DEBUG(__func__) << query << ", " << artist.name;

        cppdb::statement statement = connection() << query << artist.name;
        cppdb::result result = statement.row();

        if(result.empty()) {
            return "";
        }

        return result.get<std::string>("id");
    }

    bool ArtistMapper::update(const Artist& artist) {
        std::string query = "UPDATE artist SET name = ? WHERE id = ?";

        BOOSTER_DEBUG(__func__) << query << ", " << artist.name << ", " << artist.id;

        cppdb::statement statement = connection() << query << artist.name << artist.id << cppdb::exec;

        return statement.affected() >= 1;
    }

}   // namespace data
