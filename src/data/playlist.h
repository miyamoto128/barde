#ifndef DATA_PLAYLIST_H
#define DATA_PLAYLIST_H

#include <cppcms/view.h>

#include <data/media.h>

namespace data {

    struct PlaylistItem : public cppcms::base_content {
        std::string id;
        std::string name;
        Media image;
        std::string description;
        size_t nbSongs;
        bool enabled;
        time_t createdAt;

        PlaylistItem() : id(""), enabled(false) {}

        bool hasImage() {
            return !image.id.empty();
        }
    };

}   // namespace data
#endif  // DATA_PLAYLIST_H
