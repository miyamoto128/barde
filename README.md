# Barde

[CppCms](http://cppcms.com) web-based application.
Compose and share playlists with your friends!


# Build and execute

The web application is build and run using [docker compose](https://docs.docker.com/compose/install/).
From the project root directory, excute the command:

```sh
docker compose up -d
```

The web-application should be up and listening on http://localhost/barde.
Then, authenticate with user `test` and password `changeme`.

## Docker services

### web

The *web* service is the web front service, that forwards the HTTP requests to the FastCGI service and serves the static files.
This service relies on [nginx](https://nginx.org) reverse proxy.

### fcgi

The *fcgi* service is a FastCGI web-application, and handles all the application logic.
It is written in C++ and based on [CppCMS](https://github.com/artyom-beilis/cppcms) library.

### db

The *db* service holds the configuration data and relies on [MariaDB](https://mariadb.org/) relational database.

# Before a Production deployment

Ensure you did not keep any of the *changeme* like passwords before a Production deployment.
Those passwords can be found in these files:
- `docker-compose.yml`, database access configuration
- `docker/fcgi/config-fcgi.json`, fast-CGI service access to the database
- `docker/db/02-barde-data.sql`, default application users
